# Master in Design for Emerging Futures
# Micro Challenge 3 MDEF / SISTER LOBA
- Rita Veronica Agreda de Pazos & Francisco Flores
- MDEF 2020/21
- Institut d'Arquitectura Avançada de Catalunya, Elisava & Fab Lab Barcelona
## Table of Contents
1. [Fair Mobility and creative codesign with women collectives](#fair-mobility-and-creative-codesign-with-women-collectives)
2. [Context and Problems](#context-and-problems)
3. [Current situation](#current-situation)
4. [Sister Loba](#sister_loba)
5. [Iteration 1](#iteration-1)
6. [Iteration 2](#iteration-2)
7. [Iteration 3](#iteration-3)
8. [Final Presentation](#final-presentation)
9. [Concepts](#concepts)
10. [Skills](#skills)
## Fair Mobility and creative codesign with women collectives
Imagine a world where all people have equal rights and opportunities and where we can feel safe in every city. In this world #GenderEquality and #FairMobility are the norms...  

Since our fight is fair mobility and bridging the gender gap, we decided to create synergies and to invite women collectives as part of this challenge. We were inspired by women cyclists around the world.
## Context and problems
Saturday, we were horrified by the breaking news: **Police are investigating reports of a Brit WhatsApp group allegedly promoting a supposed American TikTok video to call April 24th “_National Rape Day_” (The Sun)**

The article emphasized Katie’s Russell -the spokesperson of Rape Crisis England &Amp in Wales- declaration: 

> “Regardless of its origins, or whether or not it was intended as a ‘joke’ at any point, the very concept of such a day is abhorrent and has caused many people, particularly women and girls, a great deal of understandable fear”.

**The idea of encouraging sexual violence is just unacceptable for us.**

Since our fights involve fair mobility and gender gap issues, the FabAcademy’s Micro-challenge 3 in the Master in Design for Emerging Futures 2020-2021 became an ideal space of reflection, co-creation and design to tackle this problem and to create synergies. 

## Current situation 
- Climate Emergency
- Car industry and CO2 emissions
- Unfair Mobility and inclusion issues
#### Women Safety is global issue. 
Globally, almost one in three women (an estimated 736 million) have experienced physical and/or sexual violence. 

Fewer than 40% of the women who experience violence seek help of any sort. Very few look to formal institutions, such as police (fewer than 10%) and health services (UN Women, 2021).

![](Images/Gender_based_violence.png)

Public transportation systems are not always well connected. Cycling is an affordable alternative to promote fair mobility, security and wellbeing. Listening to women sharing their cycling experience in the cities and involving them in the policy making process is imperative if we want to build fair and sustainable policies.
## Sister Loba
### Co-design and co-creation process
Since we decide to design using co-creation methodologies and processes, we started by interviewing teenagers and young women who love cycling or who use the bicycle as means of transportation to discover their perceptions, needs and desires. 
We talked to women from different communities and collectives: from Fab Labers , MDEFers, schools' and univesities' students, and delivery's services workers in Barcelona, to cyclist collectives like _Mujeres en Bici_ in Leon (Mexico).

![](Images/biciclot.jpg) ![](Images/Mujeres_en_bici_Leon_18.jpg)
### Concept 
**Hermanadas/ Sisterhood movement of women cyclists**

This is the story of Sister Loba’s birth, the sisterhood movement for women cyclists.

Morgane Shaban inspired us with her protective charm: a **She-Wolf**. 

The idea of reproducing a herd of women cyclist was a perfect metaphor for a sorority mouvement!

Following the teenagers and young women ideas and needs, we used their insights as invaluable information and inputs for our first iteration. 

We established the criteria that the future artifact had to fulfill:

1. Accessible
2. Affordable
3. Relevant and meaningful
4. Inclusive and eco friendly
5. A tool for generating relevant real-time data
6. Locally produced and replicable (globally distributed)
# Iteration 1
Find a way to communicate women cyclist between each other was our first challenge... 

We started by trying to understand:
1. How to communicate two or more devices. 
2. How synchronous and asynchronous communication works. 
3. How radio communication, Bluetooth, Wifi and other options allows us to send an alert message or an emoticon.
### Materials and tools
- 2 micro:bits (or [MakeCode simulator](https://makecode.microbit.org))
- [MakeCode](https://makecode.microbit.org) or [Python](https://www.python.org/downloads/) editor
- Battery pack 

We started by learning how to send emotions as alerts signals by using a [micro:bit](https://microbit.org).

Since sharing and receiving kindness is a good way to support your well-being and the well-being of your friends we learned how to program two Micro:bits when using **radio settings** and **blocks coding** to send a smile from one micro:bit to another. 

We used a project of a series created by [MakeCodemicro:bit community ](https://makecode.microbit.org) in order to offer problem-solving and prototyping activities designed to explore technology as a solution to the challenges of the Global Goals for Sustainable Development.
### Coding
The code is based on a project that learns how to use the [micro:bit's radio feature](https://makecode.microbit.org/projects/mood-radio) to share a smile. 

We followed all the steps of the project and we flashed the code in 2 micro:bits. (You can also use the MakeCode simulator online).

**Input**

![](Images/Microbit_send_a_smile.png)

**Output**

![](Images/Microbit_receive_a_smile.png)

### Coding logic
**How it works?**
1. In this project we set the radio group to 2 (Groups are like channels, so any micro:bit using the same group will get the smile. You can use any group number you like from 0-255). 
2. By pressing button A, it sended a radio text message 'smile'. It also cleared the screen so we could send another smile.
3. When it received a radio message, it showed a smile emoji on the LED display.
4. The combination of radio group and the text of the radio message sent maked up a **protocol**: a set of rules for how two devices communicate.

Just for fun we have learned to Alexandra (8 years old) and Helena (7 years old) how to send smile emoticons and also how to send a sad emoticon using the B button.

**What we have learned?**
1. How radio communication between electronic devices uses protocols to ensure that messages are routed correctly.
2. How technology and electronic communication can be used for good.
3. We found out the limitations of that technology because we had to choose our own unique radio group numbers for each pair of participants so we could send messages to our partner and not anyone else.
## Iteration 2
### Co-designing the She-Wolf
Following the same methodology and steps we learned in the Microchallenge 2, when we co-desinged Pascal: The Chameleon, we  to co-designed a She-Wolf inspired in Morgan's protective charm. 

For this iteration, we found a 3D model of a She-Wolf in the _open source_ plataform: [MakerBot Thingiverse](https://www.thingiverse.com). 

We scaled it and adapted it to our needs and we transformed it into the perfect case to contain the electronics and to protect them from the dust and rain.

We 3D printed our She-Wolf in flexible filament. The material was perfect to help us to reduce the vibrations and impacts on the electronics while cycling. 

As _Pascal_, our She-Wolf glows in the dark. 

For 3D printing we used translucid PLA flex filament and followed this steps: 

**Step 1:** Open the STL file in Cura 

**Step 2:** Verify the model touchs the plane

**Step 3:** To see the first printing point use the Slice preview command

**Step 4:** Setting Cura printing parameters 
-  Skirt/Brim Line With: 0.4
- Support Line With: 0.4
- Initial Layer Line With: 100.0
- Shell 
-  Infill: 0
-  Material 
- [x] Printing temperature: 230,
- [x] Printing temperature initial layer: 230
- [x] Initial printing temperature: 230
- [x] Final printing temperature: 230
- [x] Build plate adhesion: 0
- [x] Build plate temperature initial layer: 0
-  Supports: columns

![](Images/3D_printed_shewolf.png)

**Bike handle bar holder model for 3D printing and supports position** 

To fix the She-Wolf to the bike we found a bike handle bar holder in the same webpage. We adapted and scaled it and we decided to 3D print it in translucent PLA. 

![](Images/3d_model_bike_handler.png) ![](Images/3D_print_bike_handler_supports.jpg)

### Electronics 
After some feedbak in the working sesions with the women cyclist collectives concerning the artifact's functionality, we understood that creating an artifact that sends an alarm signal asking for help was not enough. We needed to:

1. Empower women cyclists.
2. Create a sorority mouvement to **prevent violence**. 
3. Create an artifact following the criteria of being: helpful, relevant and meaningful to them.

Up that moment, we started a long path trying to figure out how to make a **GPS Tracker**.

At the begining, we tried to use [Blynk](https://blynk.io), an app to prototype, deploy, and remotely manage connected electronic devices at any scale. It was perfect for our first babysteps in IoT since we wanted to build an accurate GPS Tracker with live location along with latitude, longitude, speed of the vehicle and add to it some emergency features like SMS with location data.

##### Materials and tools
**Hardware components:**

_For Location Tracking:_
- Arduino Uno board
- GSM800L module with support for Nano sim
- Neo6M GPS Module
- Mini 3.7V battery  
- Few connection wires
- Protoboard

_For Call and SMS Functions_
- 1 X 10K ohm Resistor
- 1 tactile push button switch

_Software:_
- Arduino IDE
- Blynk application on mobile device

_Features:_
- WiFi 
- Bluetooth 

##### Circuit Diagram:
**Connecting the GPS Module**

The NEO-6M GPS module has four pins: VCC, RX, TX, and GND. The module communicates with the Arduino via serial communication using the TX and RX pins.
 ![](Images/GPS_Neo_6m.jpg)   ![](Images/GPS_Neo_6M_labelled.jpg)  ![](Images/GPS_Neo_6M_Schematics.jpg) 

We connected the NEO-6M GPS module to the Arduino UNO board following schematic diagram:

![](Images/NEO_6M_Arduino.png) 

|  NEO-6M GPS Module	Wiring to | Arduino UNO|
| ------ | ------ |
| VCC |  5V |
| RX | TX pin defined in the software serial (pin 3)|
| TX | RX pin defined in the software serial (pin 4)|
| GND  |GND |

### Coding logic
Before coding, to obtain data from the NEO-6M GPS module, we installed the [TinyGPS++ Library](http://arduiniana.org/libraries/tinygpsplus/) (it also have some usefull examples), and to define a conection to the pins we installed the [Software Serial library](https://www.arduino.cc/en/Reference/SoftwareSerial).

**How it works?**
1. To get raw GPS data we start a serial communication with the GPS module, we used the **Software Serial Library**. With this library we initialized **serial communication**, both to see the readings on the serial monitor and to communicate with the GPS module.
2. _Input:_ We connect the **pin 4** and **pin 3** as **RX** and **TX serial pins** to establish serial communication with the GPS module. We establish the **baud rate at 9600 bps**. It listens to the GPS serial port.
3. _Output:_ When data wa received from the module, it was sent to the serial monitor. We could see the information in the Serial Monitor at **a baud rate of 9600**. We obtain a bunch of information in the **GPS standard language, NMEA**. (NMEA stands for National Marine Electronics Association, and in the world of GPS, it is a standard data format supported by GPS manufacturers).
4. By using the **TinyGPS++ Library** we convert those NMEA messages into a readable and useful format by saving the characters sequences into variables.


**We used an example of the library, we tested the GPS and it was working :)**

Since the tracking system was working, we connected the GSM800L module  with support for Nano sim** to the Arduino UNO board. 

**Connecting the GSM800L SMS Module**

We connected the GSM800L SMS module to the Arduino UNO board following schematic diagram:

![](Images/SIM800L-GSM-Pinout.jpg) 
![](Images/SIM800L_GSM_Arduino.png)

|  GSM800L SMS Module	Wiring to | Arduino UNO|
| ------ | ------ |
| VCC |  5V |
| RX | TX pin defined in the software serial (pin 9)|
| TX | RX pin defined in the software serial (pin 8)|
| GND  |GND |

### Coding logic
To define a conection to the pins we used the [Software Serial library](https://www.arduino.cc/en/Reference/SoftwareSerial) we have installed to connect the GPS module.

**How it works?**
1. The GSM800L SMS module is a device used to establish communication over a mobile network. GSM Module requires a SIM card to operate or to register a connection with the network operator or service provider.
2. _Input:_ We connect the **pin 9** and **pin 8** as **RX** and **TX serial pins** to establish serial communication with the GSM module. We establish the **baud rate at 9600 bps**. The GPS receiver obtains the data as a whole NMEA format text. Only the latitude and longitude coordinates are taken from it using the Arduino TinyGPS library. The switch input based GPS needs a manual action to operates or send SMS. It is to just send the location by ourselves. 
3. _Output:_ The GSM module sends SMS to the number specified in the code. The code is useful for GPS and GSM projects like the Sister Loba Tracker and other Arduino based live location sharing or tracking devices, similar prototypes etc.
4. By sending the location we can report an accident and send the location of accident vehicle to rescue team, etc…

![](Images/Arduino_iteration.png) 

To see the location point in a map we downloaded the **Blynk application** on my mobile. I logged in and we created a new project named GPS Tracker. We selected the device as “Arduino UNO board” and connection type as ”GSM” to get the authorization token on my registered email account to paste it into the code.

We installed the [Blynk Library](http://help.blynk.cc/en/articles/512105-how-to-install-blynk-library-for-arduino-ide) on our Arduino IDE to upload the code. 

After several trials _we failed!!! _. 

We decide to iniciate a new iteration with an Adafruit HUZZAH32 – ESP32 Feather and other tools. At least we knew how to connect the GPS module and how to obtain the data from it.

**What we have learned?**
1. How to connect a GSM module to an Arduino UNO board and a GPS module to establish serial communication between the three devices using protocols to ensure messages are routed correctly.
2. We tried to translate cordinates data (longitude and lattitude) into a graphic location in a map but we coudn't debug.
3. We found out the limitations and complexity of the GSM technology. 

## Iteration 3
### Redifining the tracking system and communication system of the She-Wolf device
We decided to keep the tracking system or location system but we needed a smaller board, an smaller protoboard, an RGB LED light, a button and to find out how to send the real time gps position to a database. 

We wanted the women cyclist of the Sister Loba's community to track theirs paths and routes and to be able to supply a timely ordered sequence of location data for further processing.

_What have we learned from the past iterations?_

- _Iteration 1:_ 

Radio Frequency Identification (RFID) is excellent and reliable indoors or in situations where close proximity to tag readers is possible, we realized that it has limited range and still requires costly readers. This technology uses electromagnetic waves to receive the signal from the targeting object to then save the location on a reader that can be looked at through specialized software. We decided to discard this technology.

- _Iteration 2:_ 

1. GPS has global coverage but can be hindered by line-of-sight issues caused by buildings and urban canyons. In any case, we decide to keep this technology for our GPS tracking system.
2. GSM Module requires a SIM card to operate or to register a connection with the network operator or service provider. Since this is not escalable and we had to deal with different providers in each country, we decide to discard this technology.

- _In this iteration we tested:_

1. The	**_If This Then That_ (commonly known as IFTTT)** service. It should allows us to program a response to events in the world of various kinds. There is a long list of kinds of events to which IFTTT can respond, all detectable via the Internet. IFTTT has partnerships with hundreds of service providers that supply event notifications to IFTTT and execute commands that implement the responses, but some event and command interfaces are just public APIs. The programs, called applets, are simple and created graphically. 

We chose this technology because it allowed us to create programs and otherwise control IFTTT with a web interface or iOS or Android application.

We used the example that sent an email to us with the geo-localization coordinates (longitude and latitude). 

![](Images/IFTTT.png)

2. **Wi-Fi**, a family of wireless network protocols, based on the IEEE 802.11 family of standards, which are commonly used for local area networking of devices and Internet access. WI-FI allows nearby digital devices to exchange data by radio waves. Since these are the most widely used computer networks in the world, used globally in home and small office networks to link desktop and laptop computers, tablet computers, smartphones, smart TVs, printers, and smart speakers together and to a wireless router to connect them to the Internet, and in wireless access points in public places like coffee shops, hotels, libraries and airports to provide the public Internet access for mobile devices we thought it was the most appropiate communication technology for the project.

##### Materials and tools
- 3D Printer
- [PLA flex filament](https://www.smartmaterials3d.com/en/flex-filament), glows in the dark
- [PLA translucid filament](https://www.smartmaterials3d.com/en)
- 3D printed mold for a button
- Platine Silicone with catalyser 

**Hardware components:**

_For Location Tracking:_
- ESP32 Adafruit HUZZAH32 Feather board
- Neo6M GPS Module
- 1 X 10K ohm Resistor
- 1 tactile push button switch
- 1 RGB LED ligh
- 1 X 100K ohm Resistor
- 1 X 150K ohm Resistor
- A 3.7V, 2000 mAh Lithium-Polymer rechargeable battery
- Few connection wires
- Protoboard
- A smartphone, or a tablet, or a computer

_Software:_
- [Arduino](https://www.arduino.cc/en/software) IDE Software
- [IFTTT](https://ifttt.com/home)
- [TinyGPS++ Library](http://arduiniana.org/libraries/tinygpsplus/) 
- [Software Serial Library](https://www.arduino.cc/en/Reference/SoftwareSerial)
- [IFTTWebhook Library](https://www.arduino.cc/reference/en/libraries/iftttwebhook/)
- [Wire Library](https://www.arduino.cc/en/reference/wire)
- [WiFi Library](https://www.arduino.cc/en/Reference/WiFi)

_Features:_
- WiFi 
- Internet connection

##### Circuit Diagram:
**Connecting the GPS Module**

![](Images/Adafruit_HUZZAH32_Feather_pinout.png) 

The NEO-6M GPS module has four pins: VCC, RX, TX, and GND. 

The module communicates with the ESP32 via serial communication using the TX and RX pins.

|  NEO-6M GPS Module	Wiring to | ESP32 Adafruit_HUZZAH32_Feather |
| ------ | ------ |
| VCC |  5V |
| RX | TX pin defined in the software serial (pin 32)|
| TX | RX pin defined in the software serial (pin 33)|
| GND  |GND |

To connect the RGB LED and to obtain the purple color, we use this reference:

![](Images/RGB_LED_resistors.jpg)

|   RGB LED light Wiring to| ESP32 Adafruit_HUZZAH32_Feather |
| ------ | ------ |
| Red soldered to a 150K ohm Resistor | (pin 16)  |
| GND | GND|
| Blue  soldered to a 100K ohm Resistor| (pin 17)|
| Green  | |

Finally we connected the tactile push button to send the location of the GPS 

|   Tactile push button Wiring to| ESP32 Adafruit_HUZZAH32_Feather |
| ------ | ------ |
| Tactile push button soldered to a 10K ohm Resistor | (pin 21)  |

![](Images/ESP32_Iteration_2.png)![](Images/ESP32_Iteration.png)

### Code
``
```
#include <TinyGPS++.h>
#define RGB_BLUE 17
#define RGB_RED 16

bool buttonPressed = false;
bool tracking = false;

#include <TinyGPS++.h>
#include <SoftwareSerial.h>

static const int RXPin = 32, TXPin = 33;
static const uint32_t GPSBaud = 9600;

float Lat;
float Lon;

long  LatI;
long  LonI;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);


#include <WiFi.h>
#include <IFTTTWebhook.h>  //   IFTTTWEBOOOK library
#include <Wire.h>


#define WIFI_SSID "GeoS9"
#define  WIFI_PASSWORD "Alex1702"

#define IFTTT_API_KEY "euLb8dHF6nGo2jawYmDQXW43umFZLVnYvR9C5bOYFGz"
#define IFTTT_EVENT_NAME "GPS_BIKE"

long ax = 413899985;
long bx = 21921792;
int cx = 3;

char a[32];
char b[12];
char c[12];

void setup() {
  ss.begin(GPSBaud);
  
  pinMode(21, INPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);

  Serial.begin(115200);

  Serial.print("Connecting to ");
  WiFi.mode(WIFI_STA);   //   create wifi station
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  // put your main code here, to run repeatedly:
  while (ss.available() > 0){
    gps.encode(ss.read());
    if (gps.location.isUpdated()){
      Serial.print("Latitude= "); 
      Lat =  gps.location.lat()*1000000;
      LatI = (int) Lat;
      Serial.print(LatI);
      Serial.print(" Longitude= ");
      Lon = gps.location.lng()*1000000;
      LonI = (int) Lon;
      Serial.println(LonI);
    }
  }

  if (digitalRead(21) == HIGH && buttonPressed == false) {
    if (tracking == false) {
      digitalWrite(16, HIGH); // Code Neopixel Encender
      digitalWrite(17, HIGH);
      tracking = true; 
    }
    else {
      digitalWrite(16, LOW); // Code Neopixel Apagar
      digitalWrite(17, LOW);
      tracking = false;
    }
    //tracking = true; 
    buttonPressed = true;

    ltoa(LatI,a,10);
    ltoa(LonI,b,10);
  
    IFTTTWebhook wh(IFTTT_API_KEY, IFTTT_EVENT_NAME);
    wh.trigger(a, b);
    Serial.println("Email has been sent");
    delay(1000);

    //digitalWrite(13, LOW);
  }
  
  if (digitalRead(21) == LOW) {
    buttonPressed = false;
  }
  delay(100);
   }
```

# Final presentation
Trought GPS Tracker, Sister Loba women cyclist provided a valuable contribution to the co-creation process. We learned about about sensors, materials, casting and molding and new ways of tackeling global issues like womens safety and fair mobility. Our solution enhances the creative skills of society as a whole and promotes an open-minded and ‘out-of-the-box’ approach to life.

![](Images/Sisterloba_components.png)
![](Images/Sisterloba_1.png)
![](Images/Sisterloba_2.png)

## Future Development opportunities
How could we display the cycling experience on a map of the city?
How could we empower women cyclist and help them to impact in policy making?

![](Images/Map.png)

# Concepts
- [x] What is a sensor
- [x] Physical, chemical, bio reactions
- [x] Closed loop systems
- [x] Complex posrt processing (computer vision)
- [x] Analog/digital read/write
- [ ] Types of actuators
- [ ] Electromagnetic outputs
- [ ] Power systems
- [ ] Power management
- [x] Design for manufacture (tolerances)
- [x] One sided mold
- [x] Two sided mold
- [x] Molding & casting applications
- [x] Materials for molding & casting
- [x] Machine molding (rough, finishing)

# Skills
- [x] Interpreting PinOut diagrams
- [x] Interpreting the serial monitor
- [x] Programming a board with sensors
- [x] Interpreting sensor data
- [x] Rate your electronics / sizing
- [x] Consumptions/amps/volts
- [x] Choose the components
- [x] Command of 3D modelling software
- [ ] Command of milling machine
- [x] Molding and casting techniques
- [x] Programming a wifi/bluetooth enabled board
- [x] Interpret network protocols
- [x] Choose hardware for your communication
- [x] How to select a library

# Links to individual pages
Made with love by [Veronica](https://gitlab.com/ritaveronica.agreda.depazos/mdef-website), [Paco](https://paco_flores.gitlab.io/mdef-2021/)  

