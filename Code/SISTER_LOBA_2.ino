
#include <TinyGPS++.h>
#define RGB_BLUE 17
#define RGB_RED 16

bool buttonPressed = false;
bool tracking = false;

#include <TinyGPS++.h>
#include <SoftwareSerial.h>

static const int RXPin = 32, TXPin = 33;
static const uint32_t GPSBaud = 9600;

float Lat;
float Lon;

long  LatI;
long  LonI;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);


#include <WiFi.h>
#include <IFTTTWebhook.h>  //   IFTTTWEBOOOK library
#include <Wire.h>


#define WIFI_SSID "GeoS9"
#define  WIFI_PASSWORD "Alex1702"

#define IFTTT_API_KEY "euLb8dHF6nGo2jawYmDQXW43umFZLVnYvR9C5bOYFGz"
#define IFTTT_EVENT_NAME "GPS_BIKE"

long ax = 413899985;
long bx = 21921792;
int cx = 3;

char a[32];
char b[12];
char c[12];

void setup() {
  ss.begin(GPSBaud);
  
  pinMode(21, INPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);

  Serial.begin(115200);

  Serial.print("Connecting to ");
  WiFi.mode(WIFI_STA);   //   create wifi station
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  // put your main code here, to run repeatedly:
  while (ss.available() > 0){
    gps.encode(ss.read());
    if (gps.location.isUpdated()){
      Serial.print("Latitude= "); 
      Lat =  gps.location.lat()*1000000;
      LatI = (int) Lat;
      Serial.print(LatI);
      Serial.print(" Longitude= ");
      Lon = gps.location.lng()*1000000;
      LonI = (int) Lon;
      Serial.println(LonI);
    }
  }

  if (digitalRead(21) == HIGH && buttonPressed == false) {
    if (tracking == false) {
      digitalWrite(16, HIGH); // Code Neopixel Encender
      digitalWrite(17, HIGH);
      tracking = true; 
    }
    else {
      digitalWrite(16, LOW); // Code Neopixel Apagar
      digitalWrite(17, LOW);
      tracking = false;
    }
    //tracking = true; 
    buttonPressed = true;

    ltoa(LatI,a,10);
    ltoa(LonI,b,10);
  
    IFTTTWebhook wh(IFTTT_API_KEY, IFTTT_EVENT_NAME);
    wh.trigger(a, b);
    Serial.println("Email has been sent");
    delay(1000);

    //digitalWrite(13, LOW);
  }
  
  if (digitalRead(21) == LOW) {
    buttonPressed = false;
  }
  delay(100);
}
